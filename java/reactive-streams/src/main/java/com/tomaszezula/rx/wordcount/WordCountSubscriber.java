package com.tomaszezula.rx.wordcount;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.time.Duration;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

class WordCountSubscriber implements Subscriber<String> {

    private static final String MESSAGE = "\rProcessed words: %d, Elapsed: %.2f seconds";

    private static final String COMMENT = "#";

    private static final String WHITE_SPACE = "[\\p{Punct}\\s]+";

    private static final int DEFAULT_MAX_ITEMS = 1;

    private final Map<String, Integer> wordCountMap = new ConcurrentHashMap<>();

    private final AtomicInteger wordCount = new AtomicInteger();

    private final List<String> stopWords;

    private final int maxLines;

    private Subscription subscription;

    private Instant start;

    WordCountSubscriber(List<String> stopWords, int maxLines) {
        this.stopWords = stopWords;
        if (maxLines > 0) {
            this.maxLines = maxLines;
        } else {
            System.out.println("Max items must be greater zero! Setting to a default value of " + DEFAULT_MAX_ITEMS);
            this.maxLines = DEFAULT_MAX_ITEMS;
        }
    }

    int getWordCount() {
        return wordCount.get();
    }

    Map<String, Integer> getWordCountMap() {
        final TreeMap<String, Integer> sortedMap = new TreeMap<>(wordCountMap);
        return Collections.unmodifiableMap(sortedMap);
    }

    @Override
    public void onSubscribe(Subscription subscription) {
        this.subscription = subscription;
        this.subscription.request(maxLines);
        this.start = Instant.now();
    }

    @Override
    public void onNext(String line) {
        if (isValid(line)) {
            final List<String> words = Stream.of(line.toLowerCase().split(WHITE_SPACE))
                    .filter(word -> word.length() > 2)
                    .filter(word -> !stopWords.contains(word))
                    .collect(Collectors.toList());

            words.forEach(word -> wordCountMap.compute(word,
                    (key, value) -> value == null ? 1 : value + 1));

            trackProgress(words.size());
        }
        subscription.request(maxLines);
    }

    @Override
    public void onError(Throwable t) {
        t.printStackTrace();
    }

    @Override
    public void onComplete() {
        System.out.println("\n\n== Top 50 ==");
        wordCountMap.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .limit(50)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new))
        .forEach((key, value) -> System.out.println(key + ": " + value));
    }

    private boolean isValid(String line) {
        return !(line == null || line.isEmpty() || line.startsWith(COMMENT));
    }

    private void trackProgress(int count) {
        final long millisElapsed = Duration.between(start, Instant.now()).toMillis();
        final double secondsElapsed = millisElapsed / 1000f;
        System.out.print(String.format(MESSAGE, wordCount.addAndGet(count), secondsElapsed));
    }
}
