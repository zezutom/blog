package com.tomaszezula.rx.wordcount;

import io.reactivex.Flowable;
import io.reactivex.functions.Consumer;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

class Main {

    private final List<String> commonWords;

    private Main() throws IOException {
        this.commonWords = Files
                .lines(getResourcePath("stopwords.txt"))
                .collect(Collectors.toList());
    }

    private void processFile(Path filePath, int maxLines) {
        Flowable.using(
                () -> new BufferedReader(new FileReader(filePath.toString())),
                reader -> Flowable.fromIterable(() -> reader.lines().iterator()),
                reader -> reader.close()
        ).subscribe(new WordCountSubscriber(commonWords, maxLines));
    }

    private static Path getResourcePath(String fileName) {
        return Paths.get((Objects.requireNonNull(Main.class.getClassLoader().getResource(fileName))).getPath());
    }

    public static void main(String[] args) throws IOException {
        new Main().processFile(getResourcePath("enwik9"), 10);
    }

}
