package com.tomaszezula.rx.basics;

import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.ReplaySubject;

import java.util.List;

public class ObservableContract {

    private void subscriptionTrigger() {
        Observable.range(0, 10).subscribe(new CustomObserver<>());
    }

    private void zeroOnNext() {
        Observable.fromIterable(List.of()).subscribe(new CustomObserver<>());
    }

    private void errorFlow() {
        Observable.fromCallable(() -> { throw new RuntimeException("Runtime failure"); })
                .subscribe(new CustomObserver<>());
    }

    private void noEmitsAfterUnsubscribe() {
        // The subscriber unsubscribes after having received 10th item
        final CustomObserver<Integer> subscriber = new CustomObserver<>(10);
        Observable.range(0, 100).subscribe(subscriber);

        //  Asking for more items..
        // The subscriber is supposed to be unsubscribed at this point. Requests for new notifications shouldn't have any effect.
        subscriber.onNext(10);
    }

    private void serialNotifications() {
        Observable.range(0, 10)
                .observeOn(Schedulers.newThread())
                .map(logEventContents())
                .observeOn(Schedulers.newThread())
                .map(logEventContents())
                .observeOn(Schedulers.newThread())
                .map(logEventContents())
                .subscribe(new CustomObserver<>());
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void noReplay() {
        PublishSubject<Integer> source = PublishSubject.create();
        source.observeOn(Schedulers.newThread()).subscribe(new CustomObserver<>());

        source.onNext(1);
        source.onNext(2);
        source.onNext(3);

        source.observeOn(Schedulers.newThread()).subscribe(new CustomObserver<>());
        source.onNext(4);

        source.onComplete();
    }

    private void withReplay() {
        ReplaySubject<Integer> source = ReplaySubject.create();
        source.observeOn(Schedulers.newThread()).subscribe(new CustomObserver<>());

        source.onNext(1);
        source.onNext(2);
        source.onNext(3);

        source.observeOn(Schedulers.newThread()).subscribe(new CustomObserver<>());
        source.onNext(4);

        source.onComplete();
    }

    private Function<Integer, Integer> logEventContents() {
        return n -> {
            System.out.println(String.format("Scheduler thread: %s, value: %d", Thread.currentThread().getName(), n));
            return n;
        };
    }

    public static void main(String[] args) {
        ObservableContract contract = new ObservableContract();

        // onNext is optional
        System.out.println("== onNext is optional ==");
        contract.zeroOnNext();

        // Completion or Error terminate Subscription
        System.out.println("== onError ==");
        contract.errorFlow();

        // Serial Notifications
        System.out.println("== Serial notifications ==");
        contract.serialNotifications();

        // Subscription triggers Notifications
        System.out.println("== Subscription triggers notifications ==");
        contract.subscriptionTrigger();

        // Graceful termination upon Unsubscribe
        System.out.println("== Graceful termination upon unsubscribe ==");
        contract.noEmitsAfterUnsubscribe();

        // Replay is not guaranteed
        System.out.println("== No replay ==");
        contract.noReplay();

        System.out.println("== With replay ==");
        contract.withReplay();

        // TODO Optional Backpressure

//        System.out.println("== Multiple observers ==");
//        contract.multipleObservers();
    }
}
