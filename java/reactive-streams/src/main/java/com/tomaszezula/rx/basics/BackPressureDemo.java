package com.tomaszezula.rx.basics;

import io.reactivex.BackpressureOverflowStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.processors.PublishProcessor;
import io.reactivex.schedulers.Schedulers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Consumer;
import java.util.stream.IntStream;

public class BackPressureDemo {


    List<String> processEagerly(int maxElements) {
        final List<String> result = initResult();

        final ConcurrentLinkedQueue<Integer> queue = new ConcurrentLinkedQueue<>();
        final Observable<Integer> o1 = Observable.range(0, maxElements).doOnNext(queue::add);
        final Observable<Integer> o2 = Observable.fromIterable(queue).doOnNext(queue::remove);

        Observable
                .zip(o1, o2, (x, y) -> String.format("[%d, %d], backlog: %d", x, y, queue.size()))
                .observeOn(Schedulers.computation())
                .subscribe(result::add, Throwable::printStackTrace);

        return Collections.unmodifiableList(result);
    }

    List<String> applyBackPressure(int maxElements) {
        final List<String> result = initResult();

        final ConcurrentLinkedQueue<Integer> queue = new ConcurrentLinkedQueue<>();
        final Flowable<Integer> f1 = Flowable.range(0, maxElements).doOnNext(queue::add);
        final Flowable<Integer> f2 = Flowable.fromIterable(queue).doOnNext(queue::remove);

        Flowable
                .zip(f1, f2, (x, y) -> String.format("[%d, %d], backlog: %d", x, y, queue.size()))
                .subscribe(result::add, Throwable::printStackTrace);

        return Collections.unmodifiableList(result);
    }

    List<Integer> dropOldest(int maxElements, int bufferCapacity) {
        final List<Integer> result = initResult();

        Flowable.range(0, maxElements)
                .onBackpressureBuffer(bufferCapacity, () -> {}, BackpressureOverflowStrategy.DROP_OLDEST)
                .observeOn(Schedulers.computation())
                .subscribe(result::add, Throwable::printStackTrace);

        return Collections.unmodifiableList(result);
    }

    void noBackPressureFail(Consumer<Throwable> errorHandler) {
        PublishProcessor<Integer> source = PublishProcessor.create();

        source.observeOn(Schedulers.computation())
                .subscribe(System.out::println, errorHandler::accept);

        IntStream.range(0, 100_000).forEach(source::onNext);
    }

    private<T> List<T> initResult() {
        return Collections.synchronizedList(new ArrayList<>());
    }
}

