package com.tomaszezula.rx.basics;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

import java.util.concurrent.atomic.AtomicInteger;

public class CustomObserver<T> implements Observer<T> {

    private final int threshold;

    private final AtomicInteger itemCounter = new AtomicInteger();

    private Disposable subscription;

    CustomObserver() {
        this(-1);
    }

    CustomObserver(int threshold) {
        this.threshold = threshold;
    }

    @Override
    public void onSubscribe(Disposable subscription) {
        this.subscription = subscription;
        log("onSubscribe");
    }

    @Override
    public void onNext(T item) {
        if (threshold > 0 && itemCounter.get() >= threshold) {
            subscription.dispose();
            log("Unsubscribed");
        } else {
            log("onNext: " + item);
            itemCounter.incrementAndGet();
        }
    }

    @Override
    public void onError(Throwable e) {
        log("onError: " + e.getMessage());
    }

    @Override
    public void onComplete() {
        log("onComplete");
    }

    private void log(String message) {
        System.out.println(Thread.currentThread().getName() + ": " + message);
    }
}
