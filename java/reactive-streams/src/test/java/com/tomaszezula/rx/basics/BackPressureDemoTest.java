package com.tomaszezula.rx.basics;

import io.reactivex.exceptions.MissingBackpressureException;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

public class BackPressureDemoTest {

    final static int MAX_ELEMENTS = 1000;

    private BackPressureDemo backPressureDemo;

    @Before
    public void setUp() {
        backPressureDemo = new BackPressureDemo();
    }

    @Test
    public void processEagerly() {
        final List<String> result = backPressureDemo.processEagerly(MAX_ELEMENTS);
        await().atMost(1000, TimeUnit.MILLISECONDS)
                .until(() -> result.size() == MAX_ELEMENTS);

        assertThat(result.stream().limit(10)).isEqualTo(
                List.of(
                        "[0, 0], backlog: 999",
                        "[1, 1], backlog: 998",
                        "[2, 2], backlog: 997",
                        "[3, 3], backlog: 996",
                        "[4, 4], backlog: 995",
                        "[5, 5], backlog: 994",
                        "[6, 6], backlog: 993",
                        "[7, 7], backlog: 992",
                        "[8, 8], backlog: 991",
                        "[9, 9], backlog: 990"
                ));
    }

    @Test
    public void applyBackPressure() {
        final List<String> result = backPressureDemo.applyBackPressure(MAX_ELEMENTS);
        await().atMost(1000, TimeUnit.MILLISECONDS)
                .until(() -> result.size() == MAX_ELEMENTS);

        assertThat(result.stream().limit(10)).isEqualTo(
                List.of(
                        "[0, 0], backlog: 127",
                        "[1, 1], backlog: 126",
                        "[2, 2], backlog: 125",
                        "[3, 3], backlog: 124",
                        "[4, 4], backlog: 123",
                        "[5, 5], backlog: 122",
                        "[6, 6], backlog: 121",
                        "[7, 7], backlog: 120",
                        "[8, 8], backlog: 119",
                        "[9, 9], backlog: 118"
                ));
    }

    @Test
    public void dropOldest() {
        final int maxElements = 1000;
        final int bufferCapacity = 1;
        final List<Integer> result = backPressureDemo.dropOldest(maxElements, bufferCapacity);
        await().atMost(maxElements, TimeUnit.MILLISECONDS);
        assertThat(result.size()).isBetween(bufferCapacity, maxElements);
    }

    @Test
    public void noBackPressureFail() {
        final AtomicInteger errorCount = new AtomicInteger();
        Consumer<Throwable> errorHandler = throwable -> {
            assertThat(throwable.getClass()).isEqualTo(MissingBackpressureException.class);
            errorCount.incrementAndGet();
        };
        backPressureDemo.noBackPressureFail(errorHandler);
        assertThat(errorCount.get()).isGreaterThan(0);
    }
}
