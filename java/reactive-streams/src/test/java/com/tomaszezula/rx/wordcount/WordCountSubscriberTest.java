package com.tomaszezula.rx.wordcount;

import io.reactivex.Emitter;
import io.reactivex.Flowable;
import org.junit.Test;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

public class WordCountSubscriberTest {

    @Test
    public void shouldCountWords_alphabetically_sorted() {
        final List<String> lines = List.of(
                "lorem",
                "ipsum",
                "dolor",
                "Dolor",
                "loReM",
                "  IPSUM ",
                "ipsUm ");

        final WordCountSubscriber subscriber = subscribe(Collections.emptyList(), lines);
        await().atMost(1000, TimeUnit.MILLISECONDS)
                .until(() -> subscriber.getWordCount() == lines.size());

        assertThat(subscriber.getWordCountMap().toString()).isEqualTo("{dolor=2, ipsum=3, lorem=2}");
    }

    @Test
    public void shouldSkipStopWords() {
        final List<String> lines = List.of(
                "A quick brown fox jumps over the lazy dog",
                "The quick brown fox jumps over a lazy dog",
                "The quick brown fox jumps over the lazy dog"
        );
        final List<String> stopWords = List.of("a", "the", "over");

        final WordCountSubscriber subscriber = subscribe(stopWords, lines);
        await().atMost(1000, TimeUnit.MILLISECONDS)
                .until(() -> subscriber.getWordCount() == 6 * lines.size());

        assertThat(subscriber.getWordCountMap().toString()).isEqualTo("{brown=3, dog=3, fox=3, jumps=3, lazy=3, quick=3}");
    }

    @Test
    public void shouldHandleEmptyInput() {
        final WordCountSubscriber subscriber = subscribe(Collections.emptyList(), Collections.emptyList());
        await().atMost(1000, TimeUnit.MILLISECONDS)
                .until(() -> subscriber.getWordCount() == 0);

        assertThat(subscriber.getWordCountMap().toString()).isEqualTo("{}");
    }

    private WordCountSubscriber subscribe(List<String> commonWords, List<String> lines) {
        WordCountSubscriber subscriber = new WordCountSubscriber(commonWords, 1);
        Flowable.generate(lines::iterator, this::pushNextLine).subscribe(subscriber);
        return subscriber;
    }

    private void pushNextLine(Iterator<String> iterator, Emitter<String> emitter) {
        final String line = nextLine(iterator);
        if (line == null) {
            emitter.onComplete();
        } else {
            emitter.onNext(line);
        }
    }

    private String nextLine(Iterator<String> iterator) {
        if (iterator.hasNext()) {
            return iterator.next();
        }
        return null;
    }
}
